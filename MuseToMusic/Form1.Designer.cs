﻿namespace MuseToMusic
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.statusLabel = new System.Windows.Forms.Label();
			this.frequencyLabel = new System.Windows.Forms.Label();
			this.museInputCombo = new System.Windows.Forms.ComboBox();
			this.channelCombo = new System.Windows.Forms.ComboBox();
			this.channelLabel = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// statusLabel
			// 
			this.statusLabel.AutoSize = true;
			this.statusLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.statusLabel.Location = new System.Drawing.Point(12, 9);
			this.statusLabel.Name = "statusLabel";
			this.statusLabel.Size = new System.Drawing.Size(58, 22);
			this.statusLabel.TabIndex = 0;
			this.statusLabel.Text = "Status";
			// 
			// frequencyLabel
			// 
			this.frequencyLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.frequencyLabel.AutoSize = true;
			this.frequencyLabel.Location = new System.Drawing.Point(526, 9);
			this.frequencyLabel.Name = "frequencyLabel";
			this.frequencyLabel.Size = new System.Drawing.Size(84, 20);
			this.frequencyLabel.TabIndex = 2;
			this.frequencyLabel.Text = "Frequency";
			// 
			// museInputCombo
			// 
			this.museInputCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.museInputCombo.FormattingEnabled = true;
			this.museInputCombo.Location = new System.Drawing.Point(92, 175);
			this.museInputCombo.Name = "museInputCombo";
			this.museInputCombo.Size = new System.Drawing.Size(212, 28);
			this.museInputCombo.TabIndex = 4;
			this.museInputCombo.SelectedIndexChanged += new System.EventHandler(this.museInputCombo_SelectedIndexChanged);
			// 
			// channelCombo
			// 
			this.channelCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.channelCombo.FormattingEnabled = true;
			this.channelCombo.Location = new System.Drawing.Point(415, 175);
			this.channelCombo.Name = "channelCombo";
			this.channelCombo.Size = new System.Drawing.Size(121, 28);
			this.channelCombo.TabIndex = 5;
			this.channelCombo.Visible = false;
			// 
			// channelLabel
			// 
			this.channelLabel.AutoSize = true;
			this.channelLabel.Location = new System.Drawing.Point(337, 178);
			this.channelLabel.Name = "channelLabel";
			this.channelLabel.Size = new System.Drawing.Size(72, 20);
			this.channelLabel.TabIndex = 6;
			this.channelLabel.Text = "Channel:";
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(676, 517);
			this.Controls.Add(this.channelLabel);
			this.Controls.Add(this.channelCombo);
			this.Controls.Add(this.museInputCombo);
			this.Controls.Add(this.frequencyLabel);
			this.Controls.Add(this.statusLabel);
			this.Name = "Form1";
			this.Text = "Form1";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
			this.Load += new System.EventHandler(this.Form1_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label statusLabel;
		private System.Windows.Forms.Label frequencyLabel;
		private System.Windows.Forms.ComboBox museInputCombo;
		private System.Windows.Forms.ComboBox channelCombo;
		private System.Windows.Forms.Label channelLabel;
	}
}

