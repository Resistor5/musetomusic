﻿using CSCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MuseToMusic
{
	public class Synth : ISampleSource
	{
		public class SineWave
		{
			public double Phase;
			public double Frequency = 440;
			public double Amplitude = 0.5;
		}

		public List<SineWave> Tones { get; private set; }

		private readonly WaveFormat waveFormat = new WaveFormat(44100, 32, 1, AudioEncoding.IeeeFloat);

		public Synth()
		{
			Tones = new List<SineWave>();
		}

		public int Read(float[] buffer, int offset, int count)
		{
			for (int i = offset; i < count; i++)
			{
				buffer[i] = 0;

				for (int j = 0; j < Tones.Count; j++)
				{
					var tone = Tones[j];

					if (tone.Phase > 1)
						tone.Phase -= 1;

					float sine = (float)(tone.Amplitude * Math.Sin(tone.Phase * Math.PI * 2));
					buffer[i] += sine;

					tone.Phase += (tone.Frequency / WaveFormat.SampleRate);
				}
			}

			return count;
		}

		public bool CanSeek
		{
			get { return false; }
		}

		public long Length
		{
			get { return 0; }
		}

		public long Position
		{
			get
			{
				return 0;
			}
			set
			{
				throw new NotSupportedException();
			}
		}

		public WaveFormat WaveFormat
		{
			get { return waveFormat; }
		}

		public void Dispose()
		{

		}
	}
}
