﻿using Bespoke.Common;
using Bespoke.Common.Osc;
using CSCore.SoundOut;
using CSCore;
using System;
using System.Linq;
using System.Net;
using System.Windows.Forms;

namespace MuseToMusic
{
	public partial class Form1 : Form
	{
		const int Port = 5000;
		OscServer oscServer;
		int messageCount = 0, errorCount = 0;

		Synth synth = new Synth();
		ISoundOut soundOut = new DirectSoundOut(100);

		// Represents data from the Muse. 
		class MuseInput
		{
			public readonly string Name;
			public readonly string Address;
			public readonly int ChannelCount;
			public readonly Func<float, float> MapRange;	// A function that maps the Muse data to a value between 0 and 1 (inclusive).

			// Default to one channel, and leaving the input range unchanged
			public MuseInput(string name, string address, int channelCount = 1)
				: this(name, address, channelCount, IdentityMapping) { }

			public MuseInput(string name, string address, Func<float, float> map)
				: this(name, address, 1, IdentityMapping) { }

			public MuseInput(string name, string address, int channelCount, Func<float, float> map)
			{
				Name = name;
				Address = address;
				ChannelCount = channelCount;
				MapRange = map;
			}

			public override string ToString() { return Name; }

			// This function returns the input unaltered.
			public static readonly Func<float, float> IdentityMapping = x => x;

			// Convert the logarithmic inputs to a value between 0 and 1. The documentation says the range is
			// between about -40 dB to 20 dB. We'll do a simple linear transform.
			public static readonly Func<float, float> FromLogMapping = x => (x + 40) / 60;
		}

		readonly MuseInput[] museInputs = 
		{
			new MuseInput("Absolute low frequencies",	"/muse/elements/low_freqs_absolute",	4, MuseInput.FromLogMapping),
			new MuseInput("Absolute delta",				"/muse/elements/delta_absolute",		4, MuseInput.FromLogMapping),
			new MuseInput("Absolute theta",				"/muse/elements/theta_absolute",		4, MuseInput.FromLogMapping),
			new MuseInput("Absolute alpha",				"/muse/elements/alpha_absolute",		4, MuseInput.FromLogMapping),
			new MuseInput("Absolute beta",				"/muse/elements/beta_absolute",			4, MuseInput.FromLogMapping),
			new MuseInput("Absolute gamma",				"/muse/elements/gamma_absolute",		4, MuseInput.FromLogMapping),
			new MuseInput("Relative delta",				"/muse/elements/delta_relative",		4),
			new MuseInput("Relative theta",				"/muse/elements/theta_relative",		4),
			new MuseInput("Relative alpha",				"/muse/elements/alpha_relative",		4),
			new MuseInput("Relative beta",				"/muse/elements/beta_relative",			4),
			new MuseInput("Relative gamma",				"/muse/elements/gamma_relative",		4),
			new MuseInput("Session delta",				"/muse/elements/delta_session_score",	4),
			new MuseInput("Session theta",				"/muse/elements/theta_session_score",	4),
			new MuseInput("Session alpha",				"/muse/elements/alpha_session_score",	4),
			new MuseInput("Session beta",				"/muse/elements/beta_session_score",	4),
			new MuseInput("Session gamma",				"/muse/elements/gamma_session_score",	4),
			new MuseInput("Blink",						"/muse/elements/blink"),
			new MuseInput("Jaw clench",					"/muse/elements/jaw_clench"),
			new MuseInput("Concentration",				"/muse/elements/experimental/concentration"),
			new MuseInput("Mellow", 					"/muse/elements/experimental/mellow")
		};


		public Form1()
		{
			InitializeComponent();
			museInputCombo.DataSource = new BindingSource(museInputs, null);
			museInputCombo.SelectedItem = museInputs.Single(x => x.Name == "Mellow");	// Make "Mellow" the default selection
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			statusLabel.Text = "Connecting...";

			// Initialize OSC server
			oscServer = new OscServer(TransportType.Tcp, IPAddress.Loopback, Port);
			oscServer.FilterRegisteredMethods = false;
			oscServer.MessageReceived += new EventHandler<OscMessageReceivedEventArgs>(oscServer_MessageReceived);
			oscServer.ReceiveErrored += new EventHandler<ExceptionEventArgs>(oscServer_ReceiveErrored);
			oscServer.ConsumeParsingExceptions = false;
			oscServer.Start();

			// Initialize tone generator
			synth.Tones.Add(new Synth.SineWave() { Amplitude = 0 });
			soundOut.Initialize(synth.ToWaveSource());
		}

		// Clamp a value to a specified range
		private float Clamp(float value, float min, float max)
		{
			return value < min ? min : (value > max ? max : value);
		}

		private void oscServer_MessageReceived(object sender, OscMessageReceivedEventArgs e)
		{
			messageCount++;

			string status = string.Format("Received {0} message{1} and {2} error{3}", messageCount, messageCount == 1 ? "" : "s", errorCount, errorCount == 1 ? "" : "s");
			statusLabel.Invoke(() => { 
				statusLabel.Text = status;

				var input = (MuseInput)museInputCombo.SelectedValue;
				int channel = (int)(channelCombo.SelectedValue ?? 0);

				if (e.Message.Address == input.Address)
				{
					synth.Tones[0].Amplitude = 0.5;
					float value = Clamp(input.MapRange((float)e.Message.Data[channel]), 0, 1);

					// map the values between 0 and 1 to frequencies between 200 and 1000 using an exponential function
					synth.Tones[0].Frequency = 200 * Math.Pow(5, value);
					frequencyLabel.Text = string.Format("Frequency: {0:0} Hz", synth.Tones[0].Frequency);

					// Start playing sound when we get the first message from the muse.
					if (soundOut.PlaybackState != PlaybackState.Playing)
						soundOut.Play();
				}
			});

		}

		private void oscServer_ReceiveErrored(object sender, ExceptionEventArgs e)
		{
			// log error
			errorCount++;

			string status = string.Format("Received {0} message{1} and {2} error{3}", messageCount, messageCount == 1 ? "" : "s", errorCount, errorCount == 1 ? "" : "s");
			statusLabel.Invoke(() =>
			{
				statusLabel.Text = status;
			});
		}

		private void Form1_FormClosing(object sender, FormClosingEventArgs e)
		{
			// Cleanup
			oscServer.Stop();
			soundOut.Dispose();
			synth.Dispose();
		}

		private void museInputCombo_SelectedIndexChanged(object sender, EventArgs e)
		{
			var input = (MuseInput)museInputCombo.SelectedValue;

			// Stop whatever sound was playing
			soundOut.Stop();

			if (input.ChannelCount > 1)
			{
				channelCombo.Items.Clear();
				for (int i = 0; i < input.ChannelCount; i++)
					channelCombo.Items.Add(i);
				
				if (channelCombo.SelectedIndex < 0)
					channelCombo.SelectedIndex = 0;

				channelCombo.Visible = channelLabel.Visible = true;
			}
			else
			{
				channelCombo.Visible = channelLabel.Visible = false;
				channelCombo.Items.Clear();
			}
		}
	}
}
